package main

import (
	"cmoesgaard/aoc2023/utils"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	filename := "../../input/2.txt"
	fmt.Println("Part A:", partA(filename))
	fmt.Println("Part B:", partB(filename))
}

func processLine(line string) (int, []string) {
	line = strings.ReplaceAll(line, ",", "")
	line = strings.ReplaceAll(line, ";", "")
	line = strings.ReplaceAll(line, ":", "")
	words := strings.Fields(line)

	gameID, _ := strconv.Atoi(words[1])
	games := words[2:]

	return gameID, games
}

func partA(filename string) int {
	sum := 0

	utils.HandleLines(filename, func(line string) {
		gameID, games := processLine(line)

		valid := true

		for i := 0; i < len(games); i += 2 {
			value, _ := strconv.Atoi(games[i])
			color := games[i+1]

			switch color {
			case "red":
				if value > 12 {
					valid = false
				}
			case "green":
				if value > 13 {
					valid = false
				}
			case "blue":
				if value > 14 {
					valid = false
				}
			}
		}

		if valid {
			sum += gameID
		}
	})

	return sum
}

func partB(filename string) int {

	sum := 0

	handler := func(line string) {
		_, games := processLine(line)

		redMax := 0
		greenMax := 0
		blueMax := 0

		for i := 0; i < len(games); i += 2 {
			value, _ := strconv.Atoi(games[i])
			color := games[i+1]

			switch color {
			case "red":
				redMax = max(value, redMax)
			case "green":
				greenMax = max(value, greenMax)
			case "blue":
				blueMax = max(value, blueMax)
			}
		}

		power := redMax * greenMax * blueMax
		sum += power

	}

	utils.HandleLines(filename, handler)

	return sum
}
