package main

import (
	"testing"
)

func TestPartA(t *testing.T) {
	expected := 8
	actual := partA("../../testdata/2.txt")

	if expected != actual {
		t.Errorf(`PartA() returned %d; want %d`, actual, expected)
	}
}

func TestPartB(t *testing.T) {
	expected := 2286
	actual := partB("../../testdata/2.txt")

	if expected != actual {
		t.Errorf(`PartB() returned %d; want %d`, actual, expected)
	}
}
