package main

import (
	"testing"
)

func TestPartA(t *testing.T) {
	expected := 142
	actual := partA("../../testdata/1_a.txt")

	if expected != actual {
		t.Errorf(`PartA() returned %d; want %d`, actual, expected)
	}
}

func TestPartB(t *testing.T) {
	expected := 281
	actual := partB("../../testdata/1_b.txt")

	if expected != actual {
		t.Errorf(`PartB() returned %d; want %d`, actual, expected)
	}
}
