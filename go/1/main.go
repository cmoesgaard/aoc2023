package main

import (
	"cmoesgaard/aoc2023/utils"
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	filename := "../../input/1.txt"
	fmt.Println("Part A:", partA(filename))
	fmt.Println("Part B:", partB(filename))
}

func partA(filename string) int {
	sum := 0

	handler := func(line string) {
		digits := make([]rune, 0)
		for _, r := range line {
			if unicode.IsDigit(r) {
				digits = append(digits, r)
			}
		}

		value := string(digits[0]) + string(digits[len(digits)-1])
		v, _ := strconv.Atoi(value)
		sum += v
	}

	utils.HandleLines(filename, handler)

	return sum
}

func partB(filename string) int {
	valueMap := map[string]rune{
		"one":   '1',
		"two":   '2',
		"three": '3',
		"four":  '4',
		"five":  '5',
		"six":   '6',
		"seven": '7',
		"eight": '8',
		"nine":  '9',
	}

	sum := 0

	utils.HandleLines(filename, func(line string) {
		digits := make([]rune, 0)

		lineLength := len(line)

		for i, r := range line {
			if unicode.IsDigit(r) {
				digits = append(digits, r)
				continue
			}
			substring := line[i:lineLength]

			for k := range valueMap {
				if strings.Index(substring, k) == 0 {
					digits = append(digits, valueMap[k])
				}
				continue
			}
		}

		value := string(digits[0]) + string(digits[len(digits)-1])
		v, _ := strconv.Atoi(value)
		sum += v
	})

	return sum
}
