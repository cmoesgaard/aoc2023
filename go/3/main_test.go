package main

import (
	"testing"
)

func TestPartA(t *testing.T) {
	expected := 4361
	actual := partA("../../testdata/3.txt")

	if expected != actual {
		t.Errorf(`PartA() returned %d; want %d`, actual, expected)
	}
}

func TestPartB(t *testing.T) {
	expected := 467835
	actual := partB("../../testdata/3.txt")

	if expected != actual {
		t.Errorf(`PartB() returned %d; want %d`, actual, expected)
	}
}
