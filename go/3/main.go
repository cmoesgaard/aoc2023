package main

import (
	"cmoesgaard/aoc2023/utils"
	"fmt"
	"regexp"
	"strconv"
	"unicode"
)

func main() {
	filename := "../../input/3.txt"
	fmt.Println("Part A:", partA(filename))
	fmt.Println("Part B:", partB(filename))
}

// A part number and the bounding box one space around it.
// Edges of the grid should have been taken into account as to not go out of bounds.
type partNumber struct {
	val  int
	xMin int
	xMax int
	yMin int
	yMax int
}

// The x/y position of a gear
type gear struct {
	x int
	y int
}

func partA(filename string) int {
	grid := utils.GetLines(filename)

	var sum int

	partNumbers := findPartNumbers(grid)

	for _, v := range partNumbers {
		symbol := checkForSymbol(v.xMin, v.xMax, v.yMin, v.yMax, grid)
		if symbol {
			sum += v.val
		}
	}

	return sum
}

func partB(filename string) int {
	grid := utils.GetLines(filename)

	var sum int
	gearMap := make(map[gear][]int)

	partNumbers := findPartNumbers(grid)

	for _, v := range partNumbers {
		gears := findGears(v.xMin, v.xMax, v.yMin, v.yMax, grid)

		for _, gear := range gears {
			gearMap[gear] = append(gearMap[gear], v.val)
		}
	}

	for _, v := range gearMap {
		if len(v) != 2 {
			continue
		}
		sum += v[0] * v[1]
	}

	return sum
}

// Finds the part numbers in the grid.
// The part numbers are returned along with a 1-space bounding box around them.
// The bounding box has taken the edges of the grid into account.
func findPartNumbers(grid []string) []partNumber {
	pattern := regexp.MustCompile(`\d+`)

	partNumbers := make([]partNumber, 0)

	for y, line := range grid {
		allIndexes := pattern.FindAllSubmatchIndex([]byte(line), -1)

		for _, loc := range allIndexes {
			yMin := max(0, y-1)
			yMax := min(len(grid)-1, y+1)
			xMin := max(0, loc[0]-1)
			xMax := min(len(line)-1, loc[1])

			val, _ := strconv.Atoi(line[loc[0]:loc[1]])
			partNumbers = append(partNumbers, partNumber{val, xMin, xMax, yMin, yMax})
		}
	}
	return partNumbers
}

// Checks the grid for the presence of symbols, bounded by the pairs of x/y min/max values.
// A symbol is anything that isn't a period (.) or a number.
func checkForSymbol(xMin, xMax, yMin, yMax int, grid []string) bool {
	for y := yMin; y <= yMax; y++ {
		for x := xMin; x <= xMax; x++ {
			v := grid[y][x]
			if string(v) != "." && !unicode.IsDigit(rune(v)) {
				return true
			}
		}
	}
	return false
}

// Finds and returns all gears in the grid, bounded by the pars of x/y min/max values
// A gear is an asterisk symbol (*).
// Returns an array of gear structs with the coordinates of each gear found.
func findGears(xMin, xMax, yMin, yMax int, grid []string) []gear {
	gears := make([]gear, 0)
	for y := yMin; y <= yMax; y++ {
		for x := xMin; x <= xMax; x++ {
			v := rune(grid[y][x])
			if string(v) == "*" {
				gears = append(gears, (gear{x, y}))
			}
		}
	}
	return gears
}
