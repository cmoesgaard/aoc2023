package utils

import (
	"bufio"
	"os"
)

func HandleLines(filename string, f func(line string)) {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		f(scanner.Text())
	}
}

func HandleEnumerateLines(filename string, f func(i int, line string)) {
	var j int
	HandleLines(filename, func(line string) {
		f(j, line)
		j++
	})
}

func GetLines(filename string) []string {
	lines := make([]string, 0)

	HandleLines(filename, func(line string) {
		lines = append(lines, line)
	})

	return lines
}
