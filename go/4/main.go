package main

import (
	"cmoesgaard/aoc2023/utils"
	"fmt"
	"math"
	"strings"
)

func main() {
	filename := "../../input/4.txt"
	fmt.Println("Part A:", partA(filename))
	fmt.Println("Part B:", partB(filename))
}

func partA(filename string) int {
	var sum int

	utils.HandleLines(filename, func(line string) {
		intersection := processLine(line)

		if matches := len(intersection); matches > 0 {
			sum += int(math.Pow(2, float64(len(intersection)-1)))
		}

	})
	return sum
}

func partB(filename string) int {
	var sum int

	// How many extra cards have been won
	// The map isn't indexed by the Card ID, but rather the index of the row it was read from
	extraCards := make(map[int]int, 0)

	utils.HandleEnumerateLines(filename, func(i int, line string) {
		intersection := processLine(line)
		cards := 1 + extraCards[i]

		if matches := len(intersection); matches > 0 {
			for j := i + 1; j <= i+matches; j++ {
				extraCards[j] += cards
			}
		}

		sum += cards
	})
	return sum
}

// Perform the necessary splitting and processing of the line
func processLine(line string) []string {
	words := strings.Split(line, "|")
	left := strings.Fields(words[0])[2:]
	right := strings.Fields(words[1])

	return findIntersection(left, right)
}

// Returns a slice of strings contained in both the left and right input slices
func findIntersection(left []string, right []string) []string {
	intersection := make([]string, 0)

	for _, leftVal := range left {
		for _, rightVal := range right {
			if leftVal == rightVal {
				intersection = append(intersection, leftVal)
			}
		}
	}

	return intersection
}
