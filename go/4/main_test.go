package main

import (
	"testing"
)

func TestPartA(t *testing.T) {
	expected := 13
	actual := partA("../../testdata/4.txt")

	if expected != actual {
		t.Errorf(`PartA() returned %d; want %d`, actual, expected)
	}
}

func TestPartB(t *testing.T) {
	expected := 30
	actual := partB("../../testdata/4.txt")

	if expected != actual {
		t.Errorf(`PartB() returned %d; want %d`, actual, expected)
	}
}
